import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {
        Person michael = new Person("Michael", "Jackson", LocalDate.now().minusYears(20), 0, 0, Gender.MALE);
        Person gabriel = new Person("Gabriel", "Jackson", LocalDate.now().minusYears(23), 0, 0, Gender.MALE);
        Person john = new Person("John", "Jackson", LocalDate.now().minusMonths(20), 0, 0, Gender.MALE);
        Person[] people = {michael, gabriel, john};
        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() > Person.ADULT_AGE)
                .forEach(p -> System.out.println(p));
        //Person[] adultsArray = adultsStream.toArray(Person[] :: new);
    }
}