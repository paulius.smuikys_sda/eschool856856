import javax.print.attribute.standard.MediaSize;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
